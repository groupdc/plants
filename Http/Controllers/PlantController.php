<?php

namespace Dcms\Plants\Http\Controllers;

use Dcms\Core\Http\Controllers\BaseController;
use Dcms\Plants\Models\Plant;
use Dcms\Plants\Models\Plantcategory;
use Dcms\Plants\Models\Plantdetail;
use Dcms\Plants\Models\Plantproperty;
use Dcms\Plants\Models\Plantpropertydetail;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;

class PlantController extends BaseController
{
    public function __construct()
    {
        $this->middleware('permission:plants-browse')->only('index');
        $this->middleware('permission:plants-add')->only(['create', 'store']);
        $this->middleware('permission:plants-edit')->only(['edit', 'update']);
        $this->middleware('permission:plants-delete')->only('destroy');
    }

    public static function QueryTree()
    {
        $tree = DB::connection('project')
            ->table('plants as node')
            ->select(
                (DB::connection("project")->raw("CONCAT( REPEAT( '-',   node.depth ), ' ', case when node.botanic = '' or node.botanic is null  then plants_language.common else concat(plants_language.common, ' - ' , node.botanic) end ) AS category")),
                "node.id",
                "node.parent_id",
                "node.depth"
            //(DB::connection("project")->raw('Concat("<img src=\'/packages/dcms/core/images/flag-",country,".png\' >") as regio'))
            )
            ->leftJoin('plants_language', 'node.id', '=', 'plants_language.plant_id')
            ->groupBy('node.id')
            ->orderBy('plants_language.common')
            ->get();

        return $tree;
    }

    public static function CategoryDropdown($models = null, $selected_id = null, $enableNull = true, $name = "parent_id", $key = "id", $value = "category")
    {
        $dropdown = "empty set";
        if (!is_null($models) && count($models) > 0) {
            $dropdown = '<select name="' . $name . '" class="form-control" id="parent_id">' . "\r\n";

            if ($enableNull == true) {
                $dropdown .= '<option value="">None</option>';
            }

            foreach ($models as $model) {
                $selected = "";
                if (!is_null($selected_id) && $selected_id == $model->$key) {
                    $selected = "selected";
                }

                $dropdown .= '<option ' . $selected . ' value="' . $model->$key . '" class="' . $name . ' parent-' . (is_null($model->parent_id) ? 0 : $model->parent_id) . ' depth-' . $model->depth . '">' . $model->$value . '</option>' . "\r\n";
            }
            $dropdown .= '</select>' . "\r\n" . "\r\n";
        }

        return $dropdown;
    }

    public function getProperties($plantid = null)
    {
        $oProperties = DB::connection('project')->select(
            "  SELECT
																plants_property.id,
																plants_property.id as property_id,
																plants_property.property_name,
																case when 1 = (select 1 from plants_property as x where x.id = plants_property.id and  x.property is not null and x.property <>'') then plants_property.property  else 	(SELECT property FROM plants_property_language WHERE property_id = plants_property.id LIMIT 1) end as property_language,
																case when exists(SELECT plant_id FROM plants_to_property WHERE plant_id =  ? AND plant_property_id = plants_property.id) then 1 else 0 end as selected
															FROM
																plants_property
															ORDER BY
																plants_property.property_name,
																4",
            [intval($plantid)]
        );

        return $oProperties;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the categories
        return View::make('dcmsplants::plants/index');
    }

    public function getDatatable()
    {
        return DataTables::queryBuilder(
            DB::connection('project')
                ->table('plants as node')
                ->select(
                    (DB::connection("project")->raw("CONCAT( REPEAT( '-', node.depth ), node.botanic) AS botanic")),
                    "node.id",
                    "plants_language.common as common",
                    (DB::connection("project")->raw('Concat("<img src=\'/packages/dcms/core/images/flag-",lcase(country),".png\' >") as country'))
                )
                ->join('plants_language', 'node.id', '=', 'plants_language.plant_id')
                ->leftjoin('languages', 'plants_language.language_id', '=', 'languages.id')
                ->orderBy('node.lft')
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/plantguide/' . $model->id . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">';

                if(Auth::user()->can('plants-edit')){
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/plantguide/' . $model->id . '/edit"><i class="fa fa-pencil"></i></a>';
                }
                if(Auth::user()->can('plants-delete')){
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this plant" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="fa fa-trash-o"></i></button>';
                }

                $edit .= '</form>';
                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }


    public function getCategories($returnType = "array")
    {
        $mCategories = Plantcategory::with([
            'plantcategorydetail' => function ($query) {
                $query->where('language_id', '=', '1');
            },
        ])->get();
        if ($returnType == "model") {
            return $mCategories;
        }

        $aCategories = [];
        foreach ($mCategories as $Category) {
            $aCategories[$Category->id] = $Category->plantcategorydetail->category . ' (pH ' . $Category->ph . ')';
        }

        return $aCategories;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languages = DB::connection("project")
            ->table("languages")
            ->select((DB::connection("project")->raw("'' as botanic, '0' as online, '' as plant_detail_id,  '' as common, '' as description, '' as image")), "id", "id as language_id", "language", "country", "language_name")->get();

        // load the create form (app/views/categories/create.blade.php)
        return View::make('dcmsplants::plants/form')
            ->with('languages', $languages)
            ->with('oProperties', $this->getProperties())
            ->with('parentplants', $this->CategoryDropdown($this->QueryTree()));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //	get the category
        $Plant = Plant::find($id);

        $languages = DB::connection("project")->select('
													SELECT language_id, languages.language, languages.country, languages.language_name, plants.botanic, plants.online, plants_language.id as plant_detail_id, plants_language.plant_id, plants_language.common,  plants_language.description, plants.image
													FROM plants_language
													LEFT JOIN plants on plants_language.plant_id = plants.id
													LEFT JOIN languages on languages.id = plants_language.language_id
													WHERE  languages.id is not null AND  plant_id = ?
													UNION
													SELECT languages.id , language, country, language_name, \'\' , 0 ,\'\' ,\'\'  ,\'\'  ,\'\' ,\'\'
													FROM languages
													WHERE id NOT IN (SELECT language_id FROM plants_language WHERE plant_id = ?) ORDER BY 1
													', [$id, $id]);

        return View::make('dcmsplants::plants/form')
            ->with('Plant', $Plant)
            ->with('languages', $languages)
            ->with('oProperties', $this->getProperties($id))
            ->with('parentplants', $this->CategoryDropdown($this->QueryTree(), $Plant->parent_id));
    }


    private function validatePlantForm()
    {
        return true;
    }

    private function savePlantProperties($plantid = null)
    {
        $newPlant = true;
        // do check if the given id is existing.
        if (!is_null($plantid) && intval($plantid) > 0) {
            $Plant = Plant::find($plantid);
        }

        if (!isset($Plant) || is_null($Plant)) {
            $Plant = new Plant;
        } else {
            $newPlant = false;
        }

        $Plant->family = request()->get('family');
        $Plant->botanic = request()->get('botanic');
        $Plant->slug = str_slug(request()->get('botanic'));
        $Plant->online = intval(request()->get('online')) === 1 ? 1 : 0;
        $Plant->image = request()->get('image');
        //	if(request()->has("category_id") && intval(request()->get("category_id"))>0 ) $Plant->category_id	= request()->get("category_id");
        //	else  $Plant->category_id	= NULL;
        $Plant->save();

        $makechild = false;
        $makeroot = false;
        $theParentid = null;

        //moving a parent up a tree is not neccesary we only sort the plants by name not by occurance in a tree $node->moveToLeftOf()
        if (intval(request()->get('parent_id')) > 0 && intval(request()->get('parent_id')) <> $Plant->parent_id && intval(request()->get('parent_id')) <> $Plant->id) {
            $makechild = true;
            $theParentid = intval(request()->get('parent_id'));
        } elseif (intval(request()->get('parent_id')) <= 0 && request()->get('parent_id') <> $Plant->parent_id && intval(request()->get('parent_id')) <> $Plant->id) {
            $makeroot = true;
        }

        if ($makeroot == true) {
            $Plant->makeRoot();
        }
        if ($makechild == true) {
            $Plant->makeChildOf($theParentid);
        }

        return $Plant;
    }

    private function savePlantDetail(Plant $Plant, $givenlanguage_id = null)
    {
        $input = request()->all();

        $Plantdetail = null;

        foreach ($input["common"] as $language_id => $title) {
            if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id)) && (strlen(trim($input['common'][$language_id])) > 0)) {
                $Plantdetail = null; // reset when in a loop
                $newDetail = true;

                if (intval($input["plant_detail_id"][$language_id]) > 0) {
                    $Plantdetail = Plantdetail::find($input["plant_detail_id"][$language_id]);
                }
                if (!isset($Plantdetail) || is_null($Plantdetail)) {
                    $Plantdetail = new Plantdetail;
                } else {
                    $newDetail = false;
                }

                $Plantdetail->language_id = $language_id;
                $Plantdetail->common = $input["common"][$language_id];
                $Plantdetail->slug = str_slug($input["common"][$language_id]);
                $Plantdetail->description = $input["description"][$language_id];
                //		if(isset($input["category_id"][$language_id]) && intval($input["category_id"][$language_id])>0 ) $Plantdetail->category_id	= $input["category_id"][$language_id];
                //		else  $Plantdetail->category_id	= NULL;

                $Plantdetail->save();
                Plant::find($Plant->id)->plantdetail()->save($Plantdetail);
            } elseif (isset($input["plant_detail_id"][$language_id]) && intval($input["plant_detail_id"][$language_id]) > 0) {
                $this->destroydetail($input["plant_detail_id"][$language_id]);
            }
        }

        return $Plantdetail;
    }

    private function savePropertyToPlant($Plant, $aProperty = [])
    {
        $Plant->plantproperty()->detach();
        debug('aprop', $aProperty);
        if (count($aProperty) > 0) {
            foreach ($aProperty as $sPropertyName => $aIndexes) {
                foreach ($aIndexes as $iProperty_id) {
                    $oProperty = Plantproperty::find($iProperty_id);

                    if (is_null($oProperty)) {
                        //create the property
                        $oProperty = new Plantproperty();
                        $oProperty->property_name = $sPropertyName;
                        $oProperty->save();

                        //what about the detail
                        $oPropertydetail = new Plantpropertydetail();
                        $oPropertydetail->language_id = 1;
                        $oPropertydetail->property_id = $oProperty->id;
                        $oPropertydetail->property = $sPropertyName . " dummyvalue";
                    }

                    //attach the new/existing property to the plant
                    $Plant->plantproperty()->attach($oProperty->id);
                }
            }
        }
    }

    public function savePlantToCondition($Plant = null)
    {
        $input = request()->all();
        $Plant->conditions()->sync($input["condition_id"]);
    }

    public function savePlantToProductInformation($Plant = null)
    {
        $input = request()->all();

        $Plant->productinformation()->detach();
        if (isset($input["information_group_id"]) && count($input["information_group_id"]) > 0) {
            foreach ($input["information_group_id"] as $i => $information_group_id) {
                $Plant->productinformation()->attach($information_group_id, ['information_group_id' => $information_group_id]);
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validatePlantForm() === true) {
            $Plant = $this->savePlantProperties();
            $this->savePlantDetail($Plant);
            $this->savePropertyToPlant($Plant, (request()->has('multiproperty_id') ? request()->get('multiproperty_id') : []));
            $this->savePlantToCondition($Plant);

            // redirect
            Session::flash('message', 'Successfully created Plant!');

            return Redirect::to('admin/plantguide');
        } else {
            return $this->validatePlantForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if ($this->validatePlantForm() === true) {
            $Plant = $this->savePlantProperties($id);
            $this->savePlantDetail($Plant);
            $this->savePropertyToPlant($Plant, (request()->has('multiproperty_id') ? request()->get('multiproperty_id') : []));
            $this->savePlantToCondition($Plant);

            // redirect
            Session::flash('message', 'Successfully updated Plant!');

            return Redirect::to('admin/plantguide');
        } else {
            return $this->validatePlantForm();
        }
    }

    public function destroydetail($id)
    {
        Plantdetail::find($id);
        Plantdetail::destroy($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        // delete
        $Plant = Plant::find($id);
        Plantdetail::where('plant_id', '=', $id)->delete();
        $Plant->destroy($id);

        // redirect
        Session::flash('message', 'Successfully deleted the Plant!');

        return Redirect::to('admin/plantguide');
    }
}
