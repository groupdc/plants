<?php

return [
    "Plants" => [
        "icon"  => "fa-tree",
        "links" => [
            ["route" => "admin/plantguide", "label" => "Plantguide", "permission" => "plants"],
            ["route" => "admin/plants/properties", "label" => "Properties", "permission" => "plants"],
        ],
    ],
];

?>
