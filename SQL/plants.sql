/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table plants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants`;

CREATE TABLE `plants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `online` tinyint(4) DEFAULT '0' COMMENT '0 offline 1 online',
  `family` varchar(255) DEFAULT NULL,
  `botanic` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `height_min` int(11) DEFAULT NULL,
  `height_max` int(11) DEFAULT NULL,
  `evergreen` tinyint(4) DEFAULT '0',
  `acidity` decimal(3,1) DEFAULT NULL,
  `acidity_tolerance` decimal(3,1) DEFAULT '0.5',
  `planting_depth_min` int(11) DEFAULT NULL,
  `planting_depth_max` int(11) DEFAULT NULL,
  `planting_spacing` int(11) DEFAULT NULL,
  `seedling_time` int(11) DEFAULT NULL,
  `harvesting_time` int(11) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `admin` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`homestead`@`%` */ /*!50003 TRIGGER `before_insert_plants` BEFORE INSERT ON `plants` FOR EACH ROW BEGIN
    SET new.uuid = uuid();
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table plants_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants_language`;

CREATE TABLE `plants_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) unsigned DEFAULT NULL,
  `plant_id` int(11) unsigned DEFAULT NULL,
  `common` varchar(250) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `description` text,
  `variations` text,
  `admin` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_plantlanguage` (`language_id`),
  KEY `FK_plantid` (`plant_id`),
  CONSTRAINT `FK_plantid` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_plantlanguage` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plants_property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants_property`;

CREATE TABLE `plants_property` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `property` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table plants_property_language
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants_property_language`;

CREATE TABLE `plants_property_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) unsigned DEFAULT NULL,
  `property_id` int(11) unsigned NOT NULL,
  `property` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `admin` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_propertyid` (`property_id`),
  KEY `FK_propertylang` (`language_id`),
  CONSTRAINT `plants_property_language_ibfk_1` FOREIGN KEY (`property_id`) REFERENCES `plants_property` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `plants_property_language_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table plants_to_advices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants_to_advices`;

CREATE TABLE `plants_to_advices` (
  `plants_id` int(11) unsigned DEFAULT NULL,
  `advices_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_plantadvice` (`plants_id`),
  KEY `FK_adviceplant` (`advices_id`) USING BTREE,
  CONSTRAINT `FK_adviceplant` FOREIGN KEY (`advices_id`) REFERENCES `advices` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_plantadvice` FOREIGN KEY (`plants_id`) REFERENCES `plants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table plants_to_property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants_to_property`;

CREATE TABLE `plants_to_property` (
  `plant_id` int(10) unsigned NOT NULL,
  `plant_property_id` int(11) unsigned NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`plant_id`,`plant_property_id`),
  KEY `FK_property` (`plant_property_id`),
  CONSTRAINT `plants_to_property_ibfk_1` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `plants_to_property_ibfk_2` FOREIGN KEY (`plant_property_id`) REFERENCES `plants_property` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table plants_tuinadvies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plants_tuinadvies`;

CREATE TABLE `plants_tuinadvies` (
  `id` int(22) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` int(22) DEFAULT NULL,
  `common_name` varchar(255) DEFAULT NULL,
  `botanic_name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `flowering_periode` varchar(255) DEFAULT NULL,
  `evergreen` varchar(255) DEFAULT NULL,
  `hardiness_zone` varchar(255) DEFAULT NULL,
  `habitat` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `ph` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `flowering_color` varchar(255) DEFAULT NULL,
  `leaf_color` varchar(255) DEFAULT NULL,
  `height_min` int(22) DEFAULT NULL,
  `height_max` int(22) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
