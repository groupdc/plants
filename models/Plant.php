<?php
namespace Dcms\Plants\Models;

use App;
use DB;
use \Baum\Node as Node ;
use Dcms\Core\Models\EloquentDefaults;

class Plant extends Node
{
    protected $connection = 'project';
    protected $table  = "plants";

    public function plantdetail()
    {
        return $this->hasMany('Dcms\Plants\Models\Plantdetail', 'plant_id', 'id');
    }

    public function productinformation()
    {
        // BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
        return $this->belongsToMany('Dcms\Products\Models\Information', 'products_information_group_to_plants', 'plant_id', 'information_id');
    }

    public function plantproperty()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Plants\Models\Plantproperty', 'plants_to_property', 'plant_id', 'plant_property_id');
    }

    public function conditions()
    {
        /*
        The first argument in belongsToMany() is the name of the class Productdata, the second argument is the name of the pivot table, followed by the name of the product_id column, and at last the name of the product_data_id column.
        */
        return $this->belongsToMany('Dcms\Conditions\Models\Conditions', 'conditions_to_plants', 'plants_id', 'conditions_id');
    }
}

class Plantdetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_language";

    public function plant()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plant', 'plant_id', 'id');
    }

    public function plantcategory()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantcategories', 'category_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}

class Plantcategory extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_categories";

    public function plantcategorydetail()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantcategorydetail', 'id');
    }

    public function plants()
    {
        return $this->hasMany('Dcms\Conditions\Models\Plant', 'category_id', 'id');
    }
}

class Plantcategorydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_categories_language";

    public function plantcategory()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantcategory', 'category_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo('Dcms\Core\Models\Languages\Language', 'language_id', 'id');
    }
}

class Plantproperty extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_property";

    public function plantpropertydetail()
    {
        return $this->hasMany('Dcms\Conditions\Models\Plantpropertydetail', 'property_id', 'id');
    }
}

class Plantpropertydetail extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "plants_property_language";

    public function Plantproperty()
    {
        return $this->belongsTo('Dcms\Conditions\Models\Plantproperty', 'property_id', 'id');
    }
}
